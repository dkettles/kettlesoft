# README #

### What is this repository for? ###
This is the repository for [my website](http://kettlesoft.net).

### Creating the Site ###

This is a standard Maven build for Scala.  Simply run 

`mvn clean install`

to generate the executable jar.  Once that has been done, you create the actual content by going to the root of the project (where you ran `mvn clean install`) and running

`java -jar .\target\kettlesoft-1.0-SNAPSHOT-jar-with-dependencies.jar`

which will create the `site` directory just under the root.

### Creating a pull request ###

1. `git branch <branch name>`
2. `git checkout <branch name>`
3. Make your changes and commit as usual
4. `git push --set-upstream origin <branch name>`
5. Go to Bitbucket and create a pull request from your branch to the branch you want your stuff on.