<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Built using Bootstrap-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="This site is about everything I'm thinking about/doing.  Themes tend to focus on software, but not to the exclusion of my other hobbies: brewing, cooking, tinkering, and complaining.">
    <meta name="author" content="David Kettlestrings">

    <!-- base favicon from https://plus.google.com/109671383935938935227/about -->
    <!-- Favicon resized using http://realfavicongenerator.net/ -->
    <link rel="icon" href="favicon.ico">

    <title>The home of kettlesoft</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="./css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./css/blog.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="./index.html">Home</a>

        </nav>
      </div>
    </div>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">Kettlesoft Blog</h1>
        <p class="lead blog-description">Everything I'm Thinking About</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title">It's Still Code</h2>
            <p class="blog-post-meta">January 5, 2016 by <a href="mailto:dkettlestrings@gmail.com">Dave</a></p>
                        <p><i>This is the second of two blog posts that I'm porting over from my internal blog at work.  I've made minor changes to ensure no trade secrets are exposed (unlikely), but also to target it towards a more general audience.  A little background: in addition to other duties, I am a software tester at work.</i></p>
            <hr>
            <p>That’s right: it’s still code.  Automated tests are just code.  Let’s say it another way: as an automated testing person (that’s the title, right?), your job is to <strong>develop software</strong>.  Sure, its goal may be slightly different from what we traditionally define as &ldquo;development&rdquo;, but in the end we write text files that a computer converts to a sequence of memory reads/writes, CPU instructions, etc.</p>

            <p>So here’s a question for you: why aren’t the best practices for writing automated tests <strong>exactly the same</strong> as those for &ldquo;software development&rdquo;?  I don’t know, but they clearly aren’t, and I can prove it.  Pick a random developer and ask to take a look at the last project they worked on (if you worked on it too, it doesn’t count).  Now without knowing a blessed thing about what is going on, start opening random files.  I’ll bet you 5-to-1 that it’s <strong>absolutely clear</strong> which ones are written for development and which ones are written for testing (even taking out the assertions).</p>

            <p>Sometimes, though, it’s harder to see where this disparity occurs.  Take <a href="http://robotframework.org/">Robot Framework</a>: no developer in their right mind would use that language (yes, it’s a language!) except as an alternative to <a href="https://en.wikipedia.org/wiki/Malbolge">Malbolge</a> or <a href="https://en.wikipedia.org/wiki/Whitespace_(programming_language)">Whitespace</a>.  Ok, maybe as an alternative to Assembly too.  You code in <a href="https://en.wikipedia.org/wiki/Tab-separated_values">TSV files</a> (and people complain about the whitespace in Python…).  I can imagine the design meeting now:</p>

            <p>Developer1: &ldquo;Writing a <a href="https://en.wikipedia.org/wiki/Lexical_analysis">lexer</a> is hard!&rdquo;</p>
            <p>Developer2: &ldquo;How about we require that you separate all tokens by tabs?&rdquo;</p>
            <p>Product Owner: &ldquo;What, make them code in TSVs?  Who is going to use that?&rdquo;</p>

            <p>Answer: testers.  Why?  Because apparently automated tests aren’t <strong>*real*</strong> code.  I could go on at some length (and may very well do so in a future post) about how ill-suited Robot is for most testing purposes, but I’ll digress for now.</p>

            <p>Instead, let’s take a look at a more reasonable alternative that has some traction around the industry: Cucumber.  If you’re unfamiliar with it, I suggest <a href="https://cucumber.io/">checking it out</a>.  I’ll give a general description here because I think it will help illustrate my point (I’m pulling this directly from the link above).</p>

            <p>First, we start by writing the test case in a &ldquo;high-level&rdquo; language (English-like):</p>

            <pre><code>
              #foo.feature
              Given I have entered 50 into the calculator
              And I have entered 70 into the calculator
              When I press add
              Then the result should be 120 on the screen
            </code></pre>

            <p>The file containing this test case is what we actually execute when we run tests.  Of course, Cucumber doesn’t come pre-loaded with your notions of calculators, add buttons, results, or screens, so we create a way to map this to actual development code using Ruby:</p>

            <pre><code>
              #gluecode.rb
              Given /I have entered (.*) into the calculator /do |n|
                calculator = Calculator.new
                calculator.push(n.to_i)
              end
              # More glue code for pressing add and getting results from the screen.
            </code></pre>

            <p>Now if we run these tests and the developers have done a good job with their Calculator implementation, it passes!  So what did we achieve?</p>

            <p>There’s no denying that we wrote a very readable description of the test.  Also, barring any criminal negligence on the part of the maintainers, the English meaning of the test should be true.  Wowsers!  Documentation that is executable and guaranteed to be consistent with the behavior of the software.  Maybe we should all start doing this immediately.</p>

            <p>I am, in fact, totally fine with anyone who wants to use this.  Heck, after doing research for this article, there may be some situations where I'll use it myself.  But there is an important point to bear in mind: what you have actually done is added a layer of indirection between the development code and the tests.  In essence, you have wrapped the API of the Calculator into a (hopefully) more human-readable API.  That’s it.  It’s just more code.</p>

            <p>It makes me wonder: If this is so great, why don’t we just <strong>develop</strong> code this way instead of only testing it this way?  Seriously, why wouldn’t we?  All you’ve supposedly done is make a better API - easily readable, writable, etc.  If it’s actually a more useful API, just make that the API to begin with.  So why not?  Why wouldn’t your traditional developer want to use this?  We’ll get there in a bit...</p>

            <p>According to the <a href="https://en.wikipedia.org/wiki/Behavior-driven_development">BDD</a> enthusiasts (tangent: why does every slightly different testing philosophy have to have its own acronym?) the big advantage to this &ldquo;human-friendly&rdquo; API is that it allows &ldquo;non-developers&rdquo; to write and read tests.  These people are supposed to include business analysts and testers.  But this makes me wonder: does it really lower the <strong>coding skills</strong> necessary to write the tests?</p>

            <p>This is clearly false if the tester has to write gluecode.rb.  All my current tests are just gluecode.rb plus some assert statements.  But the common reply is, &ldquo;No, the developers write gluecode.rb, exposing this nice API to the testers who can then just focus on test cases (foo.feature).&rdquo;  I’m all for testers being allowed to focus on test cases (I have a blog post in the works on that), but using this API has a big problem.</p>

            <p>It’s a bit tricky to explain, but I’ll do my best.  I think I can best summarize it as being a <strong>very leaky abstraction</strong>.  If you haven’t heard that term before, <a href="http://www.joelonsoftware.com/articles/LeakyAbstractions.html">read this</a>.  Seriously.  Read it.  Joel is a heck of a lot better writer and a heck of a lot smarter than I am, and he really nails the problem of leaky abstractions better than I possibly could (as an aside: read his blogs!  Most of them are old, but the themes are timeless and he’s both fun and educational).  Of course, it’s still up to me to demonstrate that Cucumber gives us a very leaky abstraction.  Here I go!</p>

            <p>It’s probably easier to just give an example.  Let’s suppose our tester/business analyst has been given the glue code and decides to write the following tests (this example comes from the fact that I work at a <a href="https://here.com">map company</a> where we do these kinds of checks)</p>

            <pre><code>
              #buildingBuildingOverlap1.feature
              Given all the buildings in Winfield, IL
              Then no buildings overlap each other
            </code></pre>

            <hr>

            <pre><p>
              #buildingRoadOverlap1.feature
              Given all the buildings in Winfield, IL
              And all roads in Winfield, IL
              Then no buildings overlap roads
            </p></pre>

            <p>Great, they pass and everything!  But something happens.  The Winfield, IL data gets corrupted, or the data is no longer legally available to be used outside of the USA (our tests could be running anywhere in the world).  So, they are told to switch to another city.  London it is!  So they start by writing</p>

            <pre><code>
              #buildingBuildingOverlap2.feature
              Given all the buildings in London, England
              Then no buildings overlap each other
            </code></pre>

            <p>But it breaks.  Careful inspection shows that it’s not the data’s fault.  Of course, we’re supposing that the tester/business analyst doesn’t code, so the problem is handed back to the developers who find that the problem is in gluecode.rb.  It loads all the buildings into memory at once!  Well, the developer goes back and reimplements the &ldquo;all building in city&rdquo; code (in gluecode.rb) to stream the data in a spatially chunked way.  Great, it works!  But now…</p>

            <pre><code>
              #buildingRoadOverlap2.feature
              Given all the buildings in London, England
              And all roads in London, England
              Then no buildings overlap roads     
            </code></pre>

            <p>Uh oh, it breaks.  First, the tester/BA calls the devs and says that they need to give the &ldquo;all roads in city&rdquo; code the same chunking treatment.  This is dutifully done, but it still doesn’t work.  Why?  Because the roads and buildings being checked need to be for the same area at the same time, and now we’re in a real pickle.  Needless to say, the &ldquo;get stuff in city&rdquo; abstractions let the data access implementation details leak through.</p>

            <p>What’s the takeaway here?  You were writing code, and hey, you had to solve coding problems.  Unfortunately, the level of abstraction in the code was so high, that you could almost forget you were trying to <strong>tell a computer how to do things</strong> (also known as programming).  This very high-level, and necessarily leaky, abstraction is why no developer would want to use this API.</p>

            <p>I don’t want to make this post much longer, but I want to point out that this realization (that tests are still code) applies more generally.  So next time you find yourself tempted to string together a bunch of Bash scripts, DLLs, regular expressions, and text files in &ldquo;well-known&rdquo; locations: ask yourself, &ldquo;would this be OK if I were writing ‘development’ code?&rdquo;</p>

            <p>My manager likes to remind me from time to time that criticism is easy, but without an alternative proposal, it’s not very useful.  So here’s my proposal:</p>

            <p>@Automated Testers: Think of yourselves as developers who just focus on slightly different problems.  Learn programming like your developer colleagues.  That means knowing and implementing concepts like data structures, algorithms, loose coupling, modularity, etc.  Write your tests in the language the development code is written in.  Do this so that...</p>

            <p>@Traditional Developers: Review the testing code.  Critique it in the same way you critique any other code going into the project.  It should meet the standards your team has established.  Focus especially on readability, documentation, and maintainability.</p>

            <p><i>Thanks to my ex-coworker Steve L. for helping me form my opinions on this subject.  And, as always, thanks to my current coworker and super-wife Carrie for restraining me from writing a rant by using her proofreading powers.</i></p>

          </div>

          <nav>
            <ul class="pager">
              <li><a href=".\growing-as-a-tester.html">Previous</a></li>
              <li><a href=".\as-a-tester-i-pt1-the-first-user.html">Next</a></li>
            </ul>
          </nav>

        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>This site is about everything I'm thinking about/doing.  Themes tend to focus on software, but not to the exclusion of my other hobbies: brewing, cooking, tinkering, and complaining.</p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
              
<li><a href=".\bear-with-me.html">December 2015</a></li>
<li><a href=".\growing-as-a-tester.html">January 2016</a></li>
<li><a href=".\refactoring-the-site.html">May 2016</a></li>
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="https://www.linkedin.com/profile/view?id=AAMAABQVu8UBLoTJgKSDo82UCRsg8DT6hyn_A4c&trk=hp-identity-name">LinkedIn</a></li>
              <li><a href="https://bitbucket.org/dkettles/">BitBucket</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
