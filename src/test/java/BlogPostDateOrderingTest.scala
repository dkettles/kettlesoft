import org.scalatest.{Matchers, FlatSpec}
import templates.{BlogPostDateOrdering, DateFormat, BlogPost}

/**
  * Created by kettlest on 5/17/2016.
  */
class BlogPostDateOrderingTest extends FlatSpec with Matchers {

  "The ordering" should "have earlier posts be less than later posts" in {

    val postA = new BlogPost("", DateFormat(10, "January", 2016), null, "", "", None)
    val postB = new BlogPost("", DateFormat(12, "January", 2016), null, "", "", None)
    val postC = new BlogPost("", DateFormat(9, "February", 2016), null, "", "", None)
    val postD = new BlogPost("", DateFormat(10, "January", 2017), null, "", "", None)

    BlogPostDateOrdering.compare(postA, postB) should be (-1)
    BlogPostDateOrdering.compare(postB, postA) should be (1)

    BlogPostDateOrdering.compare(postB, postC) should be (-1)
    BlogPostDateOrdering.compare(postC, postB) should be (1)

    BlogPostDateOrdering.compare(postC, postD) should be (-1)
    BlogPostDateOrdering.compare(postD, postC) should be (1)
  }

}
