import org.scalatest.{Matchers, FlatSpec}
import templates.DateUtils

/**
  * Created by kettlest on 5/15/2016.
  */
class DateFormatTest extends FlatSpec with Matchers {

  "DateUtils" should "convert from Strings to DateFormats" in {

    val date = DateUtils.fromString("January 6, 2015")

    date.day should be (6)
    date.month should be ("January")
    date.year should be (2015)
  }

}
