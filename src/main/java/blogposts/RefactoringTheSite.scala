package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object RefactoringTheSite extends BlogPost("Refactoring the Site",
  DateUtils.fromString("May 27, 2016"),
  Dave,
  ResourceReader.get("refactoring-the-site.html"),
  "refactoring-the-site.html",
  None)
