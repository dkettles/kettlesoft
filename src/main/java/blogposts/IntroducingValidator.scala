package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object IntroducingValidator extends BlogPost("Introducing Validator",
  DateUtils.fromString("December 26, 2015"),
  Dave,
  ResourceReader.get("introducing-validator.html"),
  "introducing-validator.html",
  None)
