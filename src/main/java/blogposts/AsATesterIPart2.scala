package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object AsATesterIPart2 extends BlogPost("As a Tester, I... (pt. 2): Chain Chain Chain",
  DateUtils.fromString("January 20, 2016"),
  Dave,
  ResourceReader.get("as-a-tester-i-pt2.html"),
  "as-a-tester-i-pt2-chain-chain-chain.html",
  None)