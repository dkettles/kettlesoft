package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object AsATesterIPart3 extends BlogPost("As a Tester, I... (pt. 3): Give Me Objects or Give Me Death",
  DateUtils.fromString("January 21, 2016"),
  Dave,
  ResourceReader.get("as-a-tester-i-pt3.html"),
  "as-a-tester-i-pt3-give-me-objects-or-give-me-death.html",
  None)
