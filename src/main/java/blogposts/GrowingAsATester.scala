package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object GrowingAsATester extends BlogPost("Growing as a Tester",
  DateUtils.fromString("January 4, 2016"),
  Dave,
  ResourceReader.get("growing-as-a-tester.html"),
  "growing-as-a-tester.html",
  None)
