package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object BearWithMe extends BlogPost("Bear With Me",
  DateUtils.fromString("December 6, 2015"),
  Dave,
  ResourceReader.get("bear-with-me.html"),
  "bear-with-me.html",
  None)
