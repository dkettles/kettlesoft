package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object ItsStillCode extends BlogPost("It's Still Code",
  DateUtils.fromString("January 5, 2016"),
  Dave,
  ResourceReader.get("its-still-code.html"),
  "its-still-code.html",
  None)