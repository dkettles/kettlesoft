package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object TheRealReason extends BlogPost("The Real Reason I Have This Site",
  DateUtils.fromString("December 8, 2015"),
  Dave, ResourceReader.get("the-real-reason-i-have-this-site.html"),
  "the-real-reason-i-have-this-site.html",
  None)