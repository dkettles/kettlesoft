package blogposts

import templates.{ResourceReader, Dave, DateUtils, BlogPost}

/**
  * Created by kettlest on 5/27/2016.
  */
object AsATesterIPart1 extends BlogPost("As a Tester, I... (pt. 1): The First User",
  DateUtils.fromString("January 19, 2016"),
  Dave,
  ResourceReader.get("as-a-tester-i-pt1.html"),
  "as-a-tester-i-pt1-the-first-user.html",
  None)
