package main

import java.io.File
import blogposts._
import org.apache.commons.io.FileUtils
import templates.{BlogHistoryCurator, BlogManager}

/** The main application that builds the final site.
  *
  * This object is what is run when you execute the jar-with-dependencies.  It deletes the folder named 'site' and
  * repopulates it with the contents to push to the file server.
  *
  * Created by kettlest on 3/21/2016.
  */
object MakeSite extends App {

  override def main(args: Array[String]) = {

    //Create directory to put the finished artifacts into
    val currentDirectory = new File(".").getCanonicalFile
    val publicationDirectory = new File(currentDirectory + File.separator + "site").getCanonicalFile
    FileUtils.deleteDirectory(publicationDirectory) // Delete it if it exists
    FileUtils.forceMkdir(publicationDirectory)

    // Copy JavaScript and CSS resources
    val cssTargetDir = new File(publicationDirectory + File.separator + "css")
    val jsTargetDir = new File(publicationDirectory + File.separator + "js")
    FileUtils.forceMkdir(cssTargetDir)
    FileUtils.forceMkdir(jsTargetDir)

    val resourceDir = new File(currentDirectory + File.separator + "src" + File.separator + "main" + File.separator + "resources")
    val jsSourceDir = new File(resourceDir + File.separator + "js")
    val cssSourceDir = new File(resourceDir + File.separator + "css")

    FileUtils.copyDirectory(jsSourceDir, jsTargetDir)
    FileUtils.copyDirectory(cssSourceDir, cssTargetDir)

    //Copy favicon
    val faviconSource = new File(resourceDir + File.separator + "favicon.ico")
    val faviconTarget = new File(publicationDirectory + File.separator + "favicon.ico")
    FileUtils.copyFile(faviconSource, faviconTarget)

    //Now grab all of the BlogPosts out of the blogposts package
    //TODO: Use reflection!
    //For now, hardcode them
    val blogPosts = List(AsATesterIPart1, AsATesterIPart2, AsATesterIPart3, BearWithMe,
      GrowingAsATester, IntroducingValidator, ItsStillCode, TheRealReason, RefactoringTheSite)

    //Order them chronologically
    val blogsInOrder = new BlogHistoryCurator(blogPosts).sortedBlogPosts

    //Create the "know-everything" context object and tell all blogs about it
    val blogManager = new BlogManager(blogsInOrder, "." + File.separator)
    blogsInOrder.foreach(blog => blog.setContext(blogManager))

    //Now convert blog posts to html and save
    blogsInOrder.foreach(
      blog => {
        FileUtils.writeByteArrayToFile(new File(publicationDirectory + File.separator + blog.address).getCanonicalFile, blog.createHTMLString().getBytes)
      }
    )

    // Create another file, index.html which is just a copy of the latest blog post.
    FileUtils.writeByteArrayToFile(new File(publicationDirectory + File.separator + "index.html").getCanonicalFile, blogsInOrder.last.createHTMLString().getBytes)

  }

}
