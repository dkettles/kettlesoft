package templates

import org.apache.commons.io.IOUtils

/**
  * Created by kettlest on 5/27/2016.
  */
object ResourceReader {

  def get(resourceName: String): String = {
    val stupidStreamCrap = getClass.getResourceAsStream(s"/blogcontent/$resourceName")
    val content = IOUtils.toString(stupidStreamCrap)
    IOUtils.closeQuietly(stupidStreamCrap)
    content
  }

}
