package templates

/** Handles the date format for blog posts.
  *
  * Created by kettlest on 3/22/2016.
  */
case class DateFormat(day: Int, month: String, year: Int) extends Ordered[DateFormat] {
  def compare(other: DateFormat) : Int = {
    val yearDiff = (this.year - other.year) * 1000
    val monthDiff = (DateUtils.months.indexOf(this.month) - DateUtils.months.indexOf(other.month)) * 40
    val dayDiff = this.day - other.day
    val totalDiff = yearDiff + monthDiff + dayDiff
    totalDiff.signum
  }

  def nextMonth(): DateFormat = {
    if (this.month == "December") {DateFormat(0, "January", this.year + 1)}
    else { DateFormat(0, DateUtils.months(DateUtils.months.indexOf(this.month) + 1), this.year)}
  }
}

object DateUtils {

  val months = List("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

  /** Create a DateFormat from a string.
    *
    * The string must be of the type "MMMMMMM DD, YYYY", where the month is spelled out, the day is one or two digits,
    * and the year is a number.
    *
    * @param dateString See above.
    * @return A DateFormat case class object.
    */

  //TODO: do I even need this?
  def fromString(dateString: String): DateFormat = {
    val splitAtSpaces = dateString.split(" ")
    assert(splitAtSpaces.size == 3, "Incorrect date format")

    val month = splitAtSpaces(0)
    assert(months.contains(month), "Incorrect date format")

    val year = splitAtSpaces(2).toInt

    val day = splitAtSpaces(1).replace(",", "").toInt

    DateFormat(day, month, year)

  }

  def toString(dateFormat: DateFormat): String = {
    s"${dateFormat.month} ${dateFormat.day}, ${dateFormat.year}"
  }

}
