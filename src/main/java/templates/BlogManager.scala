package templates

/** Ugly class to hold knowledge of all blog posts.
  *
  * An instance of this class is given to a blog post so that it can get information about itself or other blog posts
  * that should really be external to a blog post instance.  It holds references to the context in which a blog post
  * lives, which is required for a blog post in order to create a useful HTML representation of itself.
  * Created by kettlest on 4/21/2016.
  */
class BlogManager(val blogOrder: List[BlogPost], val blogPath: String) {

  def getPreviousPost(post: BlogPost): Option[BlogPost] = blogOrder.indexOf(post) match {
    case 0 => None
    case x: Int => Option(blogOrder(x - 1))
  }

  def getNextPost(post: BlogPost): Option[BlogPost] = {
    val maxIndex = blogOrder.length - 1
    blogOrder.indexOf(post) match {
      case `maxIndex` => None
      case x: Int => Option(blogOrder(x + 1))
    }
  }

  def getCanonicalPath(post: Option[BlogPost]): Option[String] = {
    post match {
      case None => None
      case _ => Some(blogPath + post.get.address)
    }
  }

  def pagerHtml(post: BlogPost): String = {
    PagerGenerator.generatePager(getCanonicalPath(getPreviousPost(post)), getCanonicalPath(getNextPost(post)))
  }

  def archiverHtml(): String = {
    Archiver.createArchive(this)
  }

}
