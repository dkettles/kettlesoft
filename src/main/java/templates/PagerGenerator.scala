package templates

/**
  * Created by kettlest on 3/22/2016.
  */
object PagerGenerator {

  def generatePager(previous: Option[String], next: Option[String]) = {
    val tagForPrevious = previous match {
      case None => ""
      case Some(_) => s"""<li><a href="${previous.get}">Previous</a></li>"""
    }

    val tagForNext = next match {
      case None => ""
      case Some(_) => s"""<li><a href="${next.get}">Next</a></li>"""
    }

    s"""<nav>
            <ul class="pager">
              $tagForPrevious
              $tagForNext
            </ul>
          </nav>"""


  }

}
