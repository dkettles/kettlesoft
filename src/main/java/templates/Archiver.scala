package templates

/**
  * Created by kettlest on 3/22/2016.
  */
object Archiver {

  def createArchive(context: BlogManager): String = {
    val history = context.blogOrder
    // Project onto month, year pairs, then group
    val monthYearGrouping = history.groupBy(bp => DateFormat(0, bp.date.month, bp.date.year))
    val groupedDates = monthYearGrouping.keys.toList.sorted

    val urlDatePairs = groupedDates.map(date => Tuple2(context.getCanonicalPath(Some(monthYearGrouping(date).head)).get, date.month + " " + date.year.toString))

    val listElements = urlDatePairs.map(pair => s"""<li><a href="${pair._1}">${pair._2}</a></li>""").fold("")((x,y) => x + "\n" + y)

    s"""<h4>Archives</h4>
            <ol class="list-unstyled">
              ${listElements}
            </ol>"""

  }



}
