package templates

/** A BlogPost represents, well, a blog post.
  *
  * For a given run of MakeSite, certain information must be present in every post.  That information is captured
  * in SiteDescription.  Note that there is still a TON of HTML still in here that scaffolds the final HTML.  The HTML
  * in this class is assumed to change with the architecture/ version of Bootstrap being used, i.e. very rarely.
  *
  * Created by kettlest on 3/21/2016.
  */
class BlogPost(val title: String, val date: DateFormat, val author: Author, val content: String, val address: String, var context: Option[BlogManager]) {
  val description = SiteDescription.description
  val siteTitle = SiteDescription.siteTitle
  val blogTitle = SiteDescription.blogTitle
  val blogDescription = SiteDescription.blogDescription
  val navItems = SiteDescription.navItems

  def setContext(newContext: BlogManager) = {
    context = Some(newContext)
  }

  def createHTMLString() : String = {
    s"""<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Built using Bootstrap-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="$description">
    <meta name="author" content="${author.longName}">

    <!-- base favicon from https://plus.google.com/109671383935938935227/about -->
    <!-- Favicon resized using http://realfavicongenerator.net/ -->
    <link rel="icon" href="favicon.ico">

    <title>$siteTitle</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="./css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./css/blog.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    $navItems

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">$blogTitle</h1>
        <p class="lead blog-description">$blogDescription</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title">$title</h2>
            <p class="blog-post-meta">${DateUtils.toString(date)} by ${author.shortName}</p>
            $content

          </div>

          ${context.get.pagerHtml(this)}

        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>$description</p>
          </div>
          <div class="sidebar-module">
            ${context.get.archiverHtml()}
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="https://www.linkedin.com/profile/view?id=AAMAABQVu8UBLoTJgKSDo82UCRsg8DT6hyn_A4c&trk=hp-identity-name">LinkedIn</a></li>
              <li><a href="https://bitbucket.org/dkettles/">BitBucket</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\\/script>')</script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
"""
  }

}
