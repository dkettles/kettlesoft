package templates

/** This object contains all of the information that gets plugged into every blog post.
  *
  * Created by kettlest on 3/22/2016.
  */
object SiteDescription {
  val description = "This site is about everything I'm thinking about/doing.  Themes tend to focus on software, but not to the exclusion of my other hobbies: brewing, cooking, tinkering, and complaining."
  val siteTitle = "The home of kettlesoft"
  val blogTitle = "Kettlesoft Blog"
  val blogDescription = "Everything I'm Thinking About"
  val navItems = NavItems.makeNavItems

}
