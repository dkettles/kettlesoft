package templates

/** What a terrible name!  Here is where given a set of BlogPosts, we can put them all together and reason about their
  * history.  That is, determine what order to put them in (so we can have "previous" and "next") as well as allow
  * "archiving" and random access to a particular point in time ("go to January 2016").
  *
  * Created by kettlest on 3/23/2016.
  */
class BlogHistoryCurator(posts: List[BlogPost]) {

  def sortedBlogPosts: List[BlogPost] = posts.sorted(BlogPostDateOrdering)

}

object BlogPostDateOrdering extends Ordering[BlogPost] {
  def compare(x: BlogPost, y: BlogPost): Int = {
    x.date.compare(y.date)
  }
}
