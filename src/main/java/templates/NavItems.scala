package templates

/** This is where the buttons in the nav bar get created.
  *
  * To add another element to the nav bar, add to items.
  *
  * Created by kettlest on 3/22/2016.
  */
object NavItems {

  case class NavItem(name: String, href: String)

  val items = List(NavItem("Home", "./index.html"))

  def makeNavItems(): String = {

    val navitemsHtml : String = items.foldLeft("")((string, item) => string ++ s"""<a class="blog-nav-item active" href="${item.href}">${item.name}</a>""" + "\n")

    s"""<div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          $navitemsHtml
        </nav>
      </div>
    </div>"""

  }

}
