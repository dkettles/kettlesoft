package templates

/** A blog post author.
  *
  * Note that longName is the one that occurs in the metadata, so you want to put in a full name.
  * The shortName is the one that appears as on the blog post in a "by 'shortName'" way.  If you want a mailto
  * on your shortname, please put it in there.
  */
sealed abstract class Author(val longName: String, val shortName: String)

case object Dave extends Author("David Kettlestrings","<a href=\"mailto:dkettlestrings@gmail.com\">Dave</a>")
case object Carrie extends Author("Caroline Kettlestrings", "Carrie")
